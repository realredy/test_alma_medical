import React, {useState, useRef } from 'react'
import './rampmap.css';
import { MapContainer, TileLayer,GeoJSON, useMapEvents, useMap } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css'; 
import datajson from '../datajson/roadMap.json'


// function MyComponent() {
//     const map = useMapEvents({
//       click: () => {
//         map.locate()
//       },
//       locationfound: (location) => {
//         console.log('location found:', location)
//       },
//     })
//     return null
//   }


   


const materials = (material) =>{
  var objeto = datajson.features; 
  var greaterTen = []; 
  for (let i = 0; i< objeto.length; i++) { 
  var currentNumber = objeto[i]; 
  if (currentNumber.properties.material === material) { 
       greaterTen.push(currentNumber) 
      } 
  }
  return greaterTen.length;
}
const woods =  materials('wood');
const metal =  materials('metal');
const fiber =  materials('fiber');
  
/*:::::::::::::::::::::::::::::::::::::::::::::::::::*/
function Rampmap() {
   const Mymap = useRef();
   const [gato, setGato] = useState(true)

   const [visivility, setVisivility] = useState([])
 
    function LocationMarker() { 
        const map = useMapEvents({
          zoomend() { 
          //  map.locate(); //optener permiso localizacion 
          //  map.flyTo([   locate.lat , locate.lng  ], 12);  
          let features = [];
                 //   map.fitBounds([
                  //     [ -23.38588307, 150.52098349],
                  //     [ -23.38582807, 150.52118258], 
                      
                  // ]); 
                  map.eachLayer( function(layer) {
                    if(layer instanceof L.Marker) {
                        if(map.getBounds().contains(layer.getLatLng())) {
                          features.push(layer.feature);
                        } 
                    } 
                  })

                  console.log('visivility: ',features);
                  setVisivility(features)
           },
          }) 
        
        return (<> 
          </> 
        )
      } 
      //   L.DomEvent.addListener(document.getElementById('glynow'), 'click', function (e) {  
      //     alert('sfd');
      //   L.DomEvent.stopPropagation(e);
      // });
  


function OurComponent() {
  const map = useMap();
  var features = []; 
           setGato(false);

      map.eachLayer( function(layer) {
        if(layer instanceof L.Marker) {
            if(map.getBounds().contains(layer.getLatLng())) {
              features.push(layer.feature);
            } 
        } 
      })  

      setVisivility(features);  
  return null
}

function glynow(){
  setGato(true);
}
//   // var map is an instance of a Leaflet map
// // this function assumes you have added markers as GeoJSON to the map
// // it will return an array of all features currently shown in the
// // active bounding region.  

 const ListFilter = () => {   
    return (<ul className="Rampmap_sidebar_inner--wrapper-ul">
      {visivility.map((person, index) => (
          <ul className="Rampmap_sidebar_inner--inner-ul"> 
            <li>Ramp No.: <b>{index}</b></li>
            <li>Locality.: <b>{person.properties.locality}</b></li>
            <li>Location.: <b>{person.properties.location}</b></li>
            <li>Type.: <b>{person.properties.type}</b></li>
            <li>Material.: <b>{person.properties.material}</b></li>
          </ul>
      ))}
      </ul>);
 }
     
     const pointerIcon = new L.Icon({
      iconUrl: process.env.PUBLIC_URL + 'boatIcon.svg',
      iconRetinaUrl:  process.env.PUBLIC_URL + 'boatIcon.svg',
      iconAnchor: [15, 10],
      popupAnchor: [10, -44],
      iconSize: [50, 65]
    });

    // console.log('objeto', datajson);
   
   function onEachCountry (feature,marker) {
        //  console.log('marcador', marker); 
         marker.bindPopup(feature.properties.locality); 
    }
 
    function pointToLayer(feature, latlng) {
     // return L.circleMarker(latlng, null); // Change marker to circle
      return L.marker(latlng, { icon: pointerIcon   }); // Change the icon to a custom icon
   }
  
  
  //  function MyComponent() {
  //    var locate;
  //   const map = useMapEvents({
  //     click: () => {
  //       // map.locate(); optener permiso localizacion
  //          locate = map.getCenter();
  //      console.log('rass: ',locate.lng);
      
  //     } 
  //   })
  //   return  ( <Marker position={[21.2,-1.2]}>
  //               <Popup>You are here</Popup>
  //             </Marker>
  //           );
            
  // }

  // function MyComponent() {
  //   const map = useMapEvent('click', () => {
  //     map.setCenter([50.5, 30.5])
  //   })
  //   return null
  // }
    
    return (
        <>
           <div id="Rampmap--wrapper">
              <div className="Rampmap_sidebar">
                  <div className="Rampmap_sidebar_inner">
                        <div className="Rampmap_sidebar_inner__wrapperTitle">
                         <img src={process.env.PUBLIC_URL + "rampLogo.svg"} alt="Ramp logo svg"/> <p>Ramp Boat info:</p>
                       
                        </div>
                        <div className="Rampmap_sidebar_inner__wrapperMaterial_resume">
                            <span>Number Ramps for Materials:</span>
                            <p>Wood: <b>{woods}</b></p> 
                            <p>Metal: <b>{metal}</b></p>
                            <p>Fiber: <b>{fiber}</b></p>
                        </div>
                        <button id="Rampmap_sidebar_inner--button-filter" onClick={glynow} >Filter Visible Rmps</button>
                        <div id="filterZoom">
                         <span className="Rampmap_sidebar_inner--name">Selected Filter : {visivility.length}</span>
                         {visivility !== null ? <ListFilter /> : 'No Ramp Visible!'}

                        </div>
                  </div>
              </div>
              <div className="Rampmap--map">
               <MapContainer id="Mmap"  style={{height:"85vh"}} center={[-23.37,150.52]} zoom={14} scrollWheelZoom={true}>
                     <GeoJSON data={datajson.features} onEachFeature={onEachCountry} pointToLayer={pointToLayer.bind(this)}  />
                    
                    <TileLayer ref={Mymap}
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    /> 
                       { gato === true ? <OurComponent /> : null}
                        <LocationMarker />
                  </MapContainer>
              </div>
           </div>
        </>
    )
} 
export default Rampmap;