import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'; 
import reportWebVitals from './reportWebVitals';
import  Route   from './Route';

 
ReactDOM.render(
  <React.StrictMode> 
    <div className="List">
      <header className="List-header">
        <a href="/">
        <img className="List-header_img" src={process.env.PUBLIC_URL + 'alma_logo.jpg'} alt="logo alma medical" />
        </a>
        <div id="List_header_wrapper_menu">
          <nav className="List--header__nav">
            <ul>
              <li><img src={process.env.PUBLIC_URL + 'map.svg'} alt="map img"/><a href="/rampmap">app map</a></li>
            </ul>
          </nav>
        </div>
         <h3 className="List-header_h3">Test: User Lists & Ramp Map, Ricardo Perez</h3> 
         </header>  
    </div>
       
    <Route /> 
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
