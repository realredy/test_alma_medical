import React, {useEffect, useState,}from 'react';
import './List.css';
import {dbo} from '../firebase';
import CreateList from '../CreateList/CreateList';
import FilluserList from '../FilluserList/FilluserList';
 
function List() {
  //only for confirm if have a data 
  const [hasdata, setHasdata] = useState(true);
  const [listdata, setListdata] = useState([]);

   /* this listenner confirm delette button, after confirm whit this id
   *  if exist in database for pass whit deleteUser the dato for be deteled */ 
  const deleteUserThis = async (e) => { 
      if(e.target.id){
        const fire = dbo.firestore();  
        let querySnapshot = await fire.collection("Userlist").doc(e.target.id).get(); 
        if(querySnapshot.data().name){
          if (window.confirm("Do you really want to delette: "+ querySnapshot.data().name)) {
            console.log('DocDta:::::',querySnapshot.data().name, e.target.id); 
            deleteUser(e.target.id, fire);
          }
        } 
      }      
  } 
   
 /* 
 * This funcition execute delete ir before is confirmed and accept
 * the box-confirm trigger for windows */
   const deleteUser = async (id, db) => { 
          db.collection("Userlist").doc(id).delete().then( function(){ 
            window.location.href="/";
        }).catch(function(error) {
            console.error("Error removing document: ", error);
        }); 
   }



useEffect(() =>{
  /*
   * Befores render, get de data and fill all the list */
  // let avan = document.getElementsByClassName('List-box-list_ul')[0];

   const fechDataFromFirebase  = async () => {
   
      const fire = dbo.firestore();  
      let containnerData = [];  
  let querySnapshot = await fire.collection("Userlist").get();  
  
  if(querySnapshot.size === 0){setHasdata(false)} else {
       //avan.innerHTML = " ";// empty inner elements before fill   
     querySnapshot.forEach( function(doc) { 
      let statusData =  doc.data(); 
       
         containnerData.push({
          id:doc.id,
          name:statusData.name,
          surname:statusData.surname,
          date:statusData.date
          });
            
           });  
          setListdata(containnerData);  
    } 
  }
  fechDataFromFirebase();
},[]);
 
  if(hasdata === false ){
    return (
      <>
      <CreateList />
      </>
    )
  }else {  
  return (
    <> 
            <div id="List-box_list">
               <ul className="List-box-list_ul"> 
                 {listdata.map((displaydata, index)=>( 
                   <li key={index} className="List-box-list_li"> 
                   <table className="List-box-list_table">
                   <tbody className="List-box-list_tbody">
                   <tr>
                   <td width="20px"><b>id</b></td>
                   <td width="150px"><b>name</b></td>
                   <td width="150px"><b>surname</b></td>
                   <td width="100px"><b>age</b></td> 
                   <td><b>action</b></td> 
                   </tr>
                   <tr>
                   <td style={{ fontSize:'10px', maxWidth:'100px', overflow:'hidden'}} >{displaydata.id}</td>
                   <td>{displaydata.name}</td>
                   <td>{displaydata.surname}</td>
                   <td>{displaydata.date}</td> 
                   <td className="List-box-list_action">
                   <button className="List-box-list_action-edit">Edit</button>
                   <button id={displaydata.id} onClick={deleteUserThis}  className="List-box-list_action-delete">Delete</button>
                   </td> 
                   </tr>
     
                   </tbody>
                   </table>
                   </li>
                 ))}
               </ul>
                <FilluserList /> 
            </div> 
    </>
  );
  }

}

export default List; 