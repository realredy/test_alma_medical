import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import List from './Home/List';
import AddUser from './AddUser/AddUser'; 
import Rampmap from './rampmap/Rampmap';
function router(){
 
    return (
        <BrowserRouter>
            
            <Route exact path="/" component={List} />

            <Route path="/addUser" component={AddUser} /> 

            <Route path="/rampmap" component={Rampmap} /> 
        </BrowserRouter>
    )
  }
export default router;