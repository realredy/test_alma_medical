import React from 'react'; 
import './CreateList.css'
const CreateList = () => {

    return (
        <>
          <div id="CreateList">
         <h1 className="CreateList-h1">No user listed here!</h1>
          <span className="CreateList-span">Click <button className="CreateList-link"><a href="/addUser">Here</a></button> to create a new user</span>
         </div>
        </>
    )
}
export default CreateList;