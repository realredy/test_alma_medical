import React, {useState} from 'react';
import {dbo} from '../firebase'; 
import './AddUser.css';

  
function AddUser() {
const [dataForm, setDataForm] = useState({
      name: '',
      surname: '',
      birthday:''
});

const filterInputs = (name) => { 
    let sendMessage = document.getElementsByClassName('AddUser--span')[0];
    if(name !== ""){
        var filter = name.replace(" ",""); 
        var re = /[<>=+/*%!?@#$`^&;:(")'}{-]/g;
        var result = re.test(filter);
        if(result === true){
            sendMessage.innerHTML = "Some charapter is not usable"; 
            return false; 
        } else {
            return true;
        } 
        }else{
        sendMessage.innerHTML = "this data is mandatory";
        return false; 
        } 
}


const sendInfo = async (e) =>{
    e.preventDefault();
   var filterName = filterInputs(dataForm.name);
   var filterSurName = filterInputs(dataForm.surname);
    if(filterName && filterSurName){ 
        const fire = dbo.firestore();  
         await fire.collection("Userlist").add({   
        date: dataForm.birthday,
        name: dataForm.name,
        surname: dataForm.surname 
        }).then(function(docRef) {  
              console.log('confirma entrada: ', docRef.id); 
             if(docRef.id !== " "){ 
                window.location.href="/";
             }
        }); 
    }//if
 
    // console.log( dataForm);
    // console.log( 'filterName: ', filterName);
    // console.log( 'filterSurName: ', filterSurName);

}
  

    return (
        <>
           <div id="AddUser--container">
               <span className="AddUser--span">Form for submit a new user</span>
             <form className="AddUser--form">
                 <input className="AddUser--form-input_textName" onChange={e => setDataForm({...dataForm, name: e.target.value})} type="text" placeholder="add name" required />
                 <input className="AddUser--form-input_surName" onChange={e => setDataForm({...dataForm, surname: e.target.value})} type="text" placeholder="add surname" required />
                 <input className="AddUser--form-input_birthday" type="date" onChange={e => setDataForm({...dataForm, birthday: e.target.value})} /> 
                 <input className="AddUser--form-input_submit" onClick={sendInfo} type="submit" value="save user" />
            </form>  

               <a href="/">← Go Back</a> 
            </div> 
        </>
    );
}

export default AddUser;